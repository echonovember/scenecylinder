//
//  GameViewController.swift
//  SceneCylinderSwift
//
//  Created by Boistov Sergei on 09.03.18.
//  Copyright © 2018 Boistov Sergei. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        scene.physicsWorld.gravity = SCNVector3Make(0, -100, 0)
        
        // create and add a camera to the scene
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        scene.rootNode.addChildNode(cameraNode)
        
        // place the camera
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 15)
        
        // create and add a light to the scene
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        // create and add an ambient light to the scene
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        // retrieve the ship node
        let ship = scene.rootNode.childNode(withName: "ship", recursively: true)!
        
        // animate the 3d object
        ship.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 1)))
        
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        // set the scene to the view
//        scnView.scene = scene

        let canScene = SCNScene(named: "art.scnassets/fan_can.scn")

        if let can = canScene?.rootNode.childNode(withName: "Cylinder", recursively: true) {
            let geometry = SCNCylinder(radius: 3, height: 17.5)
            let shape = SCNPhysicsShape(geometry: geometry, options: nil)
            can.physicsBody = SCNPhysicsBody(type: .dynamic, shape: shape)
            can.physicsBody?.mass = 1
        }

//        if let cube = canScene?.rootNode.childNode(withName: "Cube", recursively: true) {
//           w 9 h 2 d 2
//            let geometry = SCNBox(width: 9, dheight: 2, length: 2, chamferRadius: 0)
//            let shape = SCNPhysicsShape(geometry: geometry, options: nil)
//            cube.physicsBody = SCNPhysicsBody(type: .static, shape: shape)
//            cube.physicsBody?.mass = 1
//        }

        scnView.scene = canScene

        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        
        // show statistics such as fps and timing information
        scnView.showsStatistics = true
        
        // configure the view
        scnView.backgroundColor = UIColor.black
        
        // add a tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scnView.addGestureRecognizer(tapGesture)
    }
//
//    - (void)addCanToScene:(SCNScene *)scene {
//    SCNScene *canScene = [SCNScene sceneNamed:@"art.scnassets/fan_can.dae"];
//    SCNNode *can = [canScene.rootNode childNodeWithName:@"Cylinder" recursively:YES];
//    can.name = @"can";
//    SCNCylinder *canGeometry = [SCNCylinder cylinderWithRadius:3 height:17.5];
//    SCNPhysicsShape *canShape = [SCNPhysicsShape shapeWithGeometry:canGeometry options:nil];
//    can.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeDynamic shape:canShape];
//    can.physicsBody.damping = 0.f;
//    can.physicsBody.angularDamping = 0.f;
//    can.physicsBody.mass = 1;
//    //    can.physicsBody.mass = can.physicsBody.mass * 20;
//    can.position = SCNVector3Make(0, 14, 0);
//    [scene.rootNode addChildNode:can];
//    }

    @objc
    func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as! SCNView

//        SCNNode *cylinder = [scnView.scene.rootNode childNodeWithName:@"can" recursively:YES];
//        [cylinder.physicsBody applyForce:SCNVector3Make(0, cylinder.physicsBody.mass*100, 0) impulse:YES];

        guard let node = scnView.scene?.rootNode.childNode(withName: "Cylinder", recursively: true) else {
            print("no node found")
            return
        }

        print("node found")
        node.physicsBody?.applyForce(SCNVector3Make(0, 100, 0), asImpulse: true)

//        // check what nodes are tapped
//        let p = gestureRecognize.location(in: scnView)
//        let hitResults = scnView.hitTest(p, options: [:])
//        // check that we clicked on at least one object
//        if hitResults.count > 0 {
//            // retrieved the first clicked object
//            let result = hitResults[0]
//
//            // get its material
//            let material = result.node.geometry!.firstMaterial!
//
//            // highlight it
//            SCNTransaction.begin()
//            SCNTransaction.animationDuration = 0.5
//
//            // on completion - unhighlight
//            SCNTransaction.completionBlock = {
//                SCNTransaction.begin()
//                SCNTransaction.animationDuration = 0.5
//
//                material.emission.contents = UIColor.black
//
//                SCNTransaction.commit()
//            }
//
//            material.emission.contents = UIColor.red
//
//            SCNTransaction.commit()
//        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

}
