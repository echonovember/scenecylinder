//
//  GameViewController.m
//  SceneCylinder
//
//  Created by Admin on 02.06.16.
//  Copyright (c) 2016 Admin. All rights reserved.
//

#import "GameViewController.h"

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // create a new scene
    SCNScene *scene = [SCNScene scene];
    scene.physicsWorld.gravity = SCNVector3Make(0, -100, 0);

    // create and add a camera to the scene
    SCNNode *cameraNode = [SCNNode node];
    cameraNode.camera = [SCNCamera camera];
    [scene.rootNode addChildNode:cameraNode];
    
    // place the camera
    cameraNode.position = SCNVector3Make(0, 20, 100);
    
    // create and add a light to the scene
    SCNNode *lightNode = [SCNNode node];
    lightNode.light = [SCNLight light];
    lightNode.light.type = SCNLightTypeOmni;
    lightNode.position = SCNVector3Make(0, 20, 75);
    [scene.rootNode addChildNode:lightNode];
    
    // create and add an ambient light to the scene
    SCNNode *ambientLightNode = [SCNNode node];
    ambientLightNode.light = [SCNLight light];
    ambientLightNode.light.type = SCNLightTypeAmbient;
    ambientLightNode.light.color = [UIColor darkGrayColor];
    [scene.rootNode addChildNode:ambientLightNode];
    
    SCNBox *plane = [SCNBox boxWithWidth:100 height:0.1 length:100 chamferRadius:0];
    SCNNode *planeNode = [SCNNode nodeWithGeometry:plane];
    SCNPhysicsShape *planeShape = [SCNPhysicsShape shapeWithGeometry:plane options:nil];
    planeNode.position = SCNVector3Make(0, -1.5, 0);
    planeNode.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeStatic shape:planeShape];
    [scene.rootNode addChildNode:planeNode];
    
    [self addCanToScene:scene];
//    [self addCubeToScene:scene];

//    SCNNode *fan = [canScene.rootNode childNodeWithName:@"Fan" recursively:YES];
//    fan.position = SCNVector3Make(0, 0, 0);
//    fan.scale = SCNVector3Make(1, 1, 1);
//    NSLog(@"fan = %@", fan);
//    [scene.rootNode addChildNode:fan];

    // retrieve the SCNView
    SCNView *scnView = (SCNView *)self.view;
    
    // set the scene to the view
    scnView.scene = scene;
    
    // allows the user to manipulate the camera
    scnView.allowsCameraControl = YES;
        
    // show statistics such as fps and timing information
    scnView.showsStatistics = YES;

    // configure the view
    scnView.backgroundColor = [UIColor blackColor];
    
    // add a tap gesture recognizer
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    NSMutableArray *gestureRecognizers = [NSMutableArray array];
    [gestureRecognizers addObject:tapGesture];
    [gestureRecognizers addObjectsFromArray:scnView.gestureRecognizers];
    scnView.gestureRecognizers = gestureRecognizers;
}

- (void)addCanToScene:(SCNScene *)scene {
    SCNScene *canScene = [SCNScene sceneNamed:@"art.scnassets/fan_can.dae"];
    SCNNode *can = [canScene.rootNode childNodeWithName:@"Cylinder" recursively:YES];
    can.name = @"can";
    SCNCylinder *canGeometry = [SCNCylinder cylinderWithRadius:3 height:17.5];
    SCNPhysicsShape *canShape = [SCNPhysicsShape shapeWithGeometry:canGeometry options:nil];
    can.physicsBody = [SCNPhysicsBody bodyWithType:SCNPhysicsBodyTypeDynamic shape:canShape];
    can.physicsBody.damping = 0.f;
    can.physicsBody.angularDamping = 0.f;
    can.physicsBody.mass = 1;
//    can.physicsBody.mass = can.physicsBody.mass * 20;
    can.position = SCNVector3Make(0, 14, 0);
    [scene.rootNode addChildNode:can];
}

- (void) handleTap:(UIGestureRecognizer*)gestureRecognize
{
    // retrieve the SCNView
    SCNView *scnView = (SCNView *)self.view;
    
    SCNNode *cylinder = [scnView.scene.rootNode childNodeWithName:@"can" recursively:YES];
    [cylinder.physicsBody applyForce:SCNVector3Make(0, cylinder.physicsBody.mass*100, 0) impulse:YES];
    
    // check what nodes are tapped
//    CGPoint p = [gestureRecognize locationInView:scnView];
//    NSArray *hitResults = [scnView hitTest:p options:nil];
//    
//    // check that we clicked on at least one object
//    if([hitResults count] > 0){
//        // retrieved the first clicked object
//        SCNHitTestResult *result = [hitResults objectAtIndex:0];
//        
//        // get its material
//        SCNMaterial *material = result.node.geometry.firstMaterial;
//        
//        // highlight it
//        [SCNTransaction begin];
//        [SCNTransaction setAnimationDuration:0.5];
//        
//        // on completion - unhighlight
//        [SCNTransaction setCompletionBlock:^{
//            [SCNTransaction begin];
//            [SCNTransaction setAnimationDuration:0.5];
//            
//            material.emission.contents = [UIColor blackColor];
//            
//            [SCNTransaction commit];
//        }];
//        
//        material.emission.contents = [UIColor redColor];
//        
//        [SCNTransaction commit];
//    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
